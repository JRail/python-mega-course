# Python Mega Course

Python Mega Course: Build 10 Real World Applications

## Changelog

### 6/1/2019
removed `env` from `.gitignore` because the current build of PyInstaller throws an error when using virtual env. This was fixed by updating `PyInstaller/depend/bindepend.py`.

https://github.com/Loran425/pyinstaller/commit/14b6e65642e4b07a4358bab278019a48dedf7460?diff=unified


### 6/6/2019
pip install opencv-python did not appear to work, so a .whl file was used instead. .whl files are stored in `pip-whls`.


### 6/1/2019
Added single file executable.

### 5/27/2019
removed `venv` as the virtualenv and created and use `env` instead. These where to steps used:

1. Create the virtual enviroment 
`py -m venv env`
2. Activate the vitrual enviroment
`.\env\Scripts\activate`
3. Check to see if virtual enviroment was set up correctly
`where python`
`.../env/bin/python.exe`

#### Notes:
- TODO: Upgrade to use `pipenv` instead of `virtualenv` (which we currently use). 
- When this virtual enviroment is active `pip` will install packages to this enviroment. 
- leave the virtual env with `deactivate`
- use `pip freeze > requirements.txt` to update the required packets that need to be installed.
- for more go to https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/
- use `pyinstaller --onefile --windowed --name nameofexecutable pythonfilename.py` to create a one file executable that does not show a commandline interface in the background. 