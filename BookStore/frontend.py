from tkinter import *
from backend import Database

# Create the database
database = Database()

class Window(object):

	def __init__(self, window):
		self.window = window

		self.window.wm_title("BookStore")

		### Top area ###
		lbl_title = Label(window, text="Title")
		lbl_title.grid(row=0, column=0)

		lbl_author = Label(window, text="Author")
		lbl_author.grid(row=0, column=2)

		lbl_year = Label(window, text="Year")
		lbl_year.grid(row=1, column=0)

		lbl_isbn = Label(window, text="ISBN")
		lbl_isbn.grid(row=1, column=2)

		self.title_text = StringVar()
		self.ent_title = Entry(window, textvariable=self.title_text)
		self.ent_title.grid(row=0, column=1)

		self.author_text = StringVar()
		self.ent_author = Entry(window, textvariable=self.author_text)
		self.ent_author.grid(row=0, column=3)

		self.year_text = StringVar()
		self.ent_year = Entry(window, textvariable=self.year_text)
		self.ent_year.grid(row=1, column=1)

		self.isbn_text = StringVar()
		self.ent_isbn = Entry(window, textvariable=self.isbn_text)
		self.ent_isbn.grid(row=1, column=3)


		### Results view ###
		# List box to display query results
		self.lb_results = Listbox(window, height=6, width=35)
		self.lb_results.grid(row=2, column=0, rowspan=6, columnspan=2)  # occupy total of 12 cells
		# Create scrollbar
		sb_results = Scrollbar(window)
		sb_results.grid(row=2, column=2, rowspan=6)
		# Configure scrollbar
		self.lb_results.configure(yscrollcommand=sb_results.set)
		sb_results.configure(command=self.lb_results.yview)
		self.lb_results.bind('<<ListboxSelect>>', self.get_selected_row)  # bind event to listbox

		### Right side buttons ###
		btn_view_all = Button(window, text="View All", width=12, command=self.view_command)
		btn_view_all.grid(row=2, column=3)

		btn_search_entry = Button(window, text="Search Entry", width=12, command=self.search_command)
		btn_search_entry.grid(row=3, column=3)

		btn_add_entry = Button(window, text="Add Entry", width=12, command=self.add_command)
		btn_add_entry.grid(row=4, column=3)

		btn_update = Button(window, text="Update", width=12, command=self.update_command)
		btn_update.grid(row=5, column=3)

		btn_delete = Button(window, text="Delete", width=12, command=self.delete_command)
		btn_delete.grid(row=6, column=3)

		btn_close = Button(window, text="Close", width=12, command=window.destroy)
		btn_close.grid(row=7, column=3)

	def get_selected_row(self, event):
		global selected_tuple
		try:
			# Grab index 0 of the cur tuple
			index = self.lb_results.curselection()[0]
			selected_tuple = self.lb_results.get(index)
			self.ent_title.delete(0, END)
			self.ent_title.insert(END, selected_tuple[1])
			self.ent_author.delete(0, END)
			self.ent_author.insert(END, selected_tuple[2])
			self.ent_year.delete(0, END)
			self.ent_year.insert(END, selected_tuple[3])
			self.ent_isbn.delete(0, END)
			self.ent_isbn.insert(END, selected_tuple[4])
		except IndexError:
			pass

	def view_command(self):
		self.lb_results.delete(0, END)
		for row in database.view():
			# Insert new row at the end of the list box
			self.lb_results.insert(END, row)

	def search_command(self):
		self.lb_results.delete(0, END)
		for row in database.search(self.title_text.get(), self.author_text.get(), self.year_text.get(), self.isbn_text.get()):
			self.lb_results.insert(END, row)

	def add_command(self):
		database.insert(self.title_text.get(), self.author_text.get(), self.year_text.get(), self.isbn_text.get())
		self.lb_results.delete(0, END)
		self.lb_results.insert(0, (self.title_text.get(), self.author_text.get(), self.year_text.get(), self.isbn_text.get()))

	def delete_command(self):
		# Assume the book id is at index 0
		database.delete(selected_tuple[0])
		
	def update_command(self):
		database.update(selected_tuple[0], self.ent_title.get(), self.ent_author.get(), self.ent_year.get(), self.ent_isbn.get())



if __name__ == "__main__":
	window = Tk()
	Window(window)
	window.mainloop()