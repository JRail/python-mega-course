import cv2
import os.path

photo_1 = 'Computer Vision\\photo.jpg'
photo_2 = 'Computer Vision\\news.jpg'
haarcascade = 'Computer Vision\\haarcascade_frontalface_default.xml'

if not os.path.isfile(photo_1):
    raise OSError('photo_1 not found:{}'.format(photo_1))
if not os.path.isfile(photo_2):
    raise OSError('photo_2 not found:{}'.format(photo_2))
if not os.path.isfile(haarcascade):
    raise OSError('Cascade not found:{}'.format(haarcascade))

face_cascade = cv2.CascadeClassifier(haarcascade)

img = cv2.imread(photo_2)
gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

faces = face_cascade.detectMultiScale(
    gray_img,
    # Higher scale factor less features that can be detected
    scaleFactor=1.1,
    minNeighbors=5
)

for x, y, w, h in faces:
    # Draw rectange on image
    img = cv2.rectangle(
        img=img,
        pt1=(x, y),
        pt2=(x + w, y + h),
        color=(0, 255, 0),
        thickness=3,
        # lineType=1,
        # shift=0
    )


print('faces type:"{}"'.format(type(faces)))
print('faces:"{}"'.format(faces))


resized = cv2.resize(img, (int(img.shape[1]/3), int(img.shape[0]/3)))
cv2.imshow("Gray", resized)
cv2.waitKey(0)
cv2.destroyAllWindows()