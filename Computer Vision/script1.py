import cv2

path = 'Playground\\Computer Vision\\'
img = cv2.imread('{}galaxy.jpg'.format(path), 0)

print(type(img))
print(img)
print(img.shape)
print(img.ndim)

# Resize the image to half of its original size
scale = 0.5
w = int(img.shape[0]*scale)
h = int(img.shape[1]*scale)
resize_image = cv2.resize(img, (h, w))

# Show the image
cv2.imshow('Galaxy', resize_image)
cv2.waitKey(5000)  # wait 5 sec or until key press
cv2.destroyAllWindows()

# Resize the image
cv2.imwrite('{}galaxy_resize.jpg'.format(path), resize_image)