import cv2
import time

# capture video. index indicates which camera you will use,
# or pass in a path to video file
video = cv2.VideoCapture(0)

# wait a moment to trigger the camera
time.sleep(3)

frame_count = 0
while True:
    check, frame = video.read()
    frame_count += 1

    print('frame #{}'.format(frame_count))
    # print('check:{}'.format(check))
    # print('frame:{}'.format(frame))

    gray_img = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # time.sleep(1)
    cv2.imshow('Capturing', gray_img)

    # press 'q' key to close window
    key = cv2.waitKey(1)  # press any button to close the window
    if key is ord('q'):
        break

video.release()  # release the video
cv2.destroyAllWindows()  # close/destroy all windows
