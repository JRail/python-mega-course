class Account:
	"""Base class for all accounts"""

	def __init__(self, filepath):
		# open file in read mode
		self.filepath = filepath
		self.balance = 1000  # default value

		try:
			with open(filepath, 'r') as file:
				self.balance = int(file.read())
		except ValueError:
			print("No balance... Using default of 1000.")
		except FileNotFoundError:
			print("Cannot access balance... Using default of 1000.")

	def widthdraw(self, amount):
		if (self.balance < amount): self.balance = 0
		else: self.balance = self.balance - amount

	def deposite(self, amount):
		self.balance = self.balance + amount

	def commit(self):
		try:
			with open(self.filepath, 'w') as file:
				file.write(str(self.balance))
		except FileNotFoundError:
			print("Cannot commit balance...")


"""
Inheritace works by passing in the base class as a param to
the child class, and calling the __init__ method of the base
class in the child __init__ method
"""
class Checking(Account):
	"""Example of doc string. This class creates a checking account"""	
	type = "checking"

	def __init__(self, filepath, fee):
		self.fee = fee
		Account.__init__(self, filepath)

	def transfer(self, amount):
		self.balance = self.balance - amount - self.fee



if __name__ == "__main__":

	print("*** Checking ***")
	checking = Checking("Playground\\Account\\balance.txt", fee=10)
	checking.deposite(100)
	checking.transfer(450)
	print(checking.balance)

	print("*** Account ***")
	account = Account("Playground\\Account\\balance.txt")
	print(account.balance)
	account.deposite(229)
	print(account.balance)
	account.widthdraw(200)
	print(account.balance)
	account.commit()