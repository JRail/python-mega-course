# Database using PostSQL
import psycopg2

CONN_INFO = "dbname='database1' user='postgres' password='postgres' host='localhost' port='5432'"

def create_table():
	conn = psycopg2.connect(CONN_INFO)
	cur = conn.cursor()
	cur.execute("CREATE TABLE IF NOT EXISTS store (item TEXT, quantity INTEGER, price REAL)")
	conn.commit()
	conn.close()

def insert(item, quantity, price):
	conn = psycopg2.connect(CONN_INFO)
	cur = conn.cursor()
	cur.execute("INSERT INTO store VALUES(%s,%s,%s)", (item, quantity, price))
	conn.commit()
	conn.close()

def view():
	conn = psycopg2.connect(CONN_INFO)
	cur = conn.cursor()
	cur.execute("SELECT * FROM store")
	rows = cur.fetchall()
	conn.close()
	return rows

def delete(item):
	conn = psycopg2.connect(CONN_INFO)
	cur = conn.cursor()
	cur.execute("DELETE FROM store WHERE item=%s",(item,))
	conn.commit()
	conn.close()

def update(item, quantity, price):
	conn = psycopg2.connect(CONN_INFO)
	cur = conn.cursor()
	cur.execute("UPDATE store SET quantity=%s, price=%s WHERE item=%s",(quantity, price, item))
	conn.commit()
	conn.close()



create_table()
print (view())
print("*** Start ***")

insert("coffee", 1, 5)
insert("water", 2, 2)
insert("orange", 3, 3)
insert("tea", 1, 3)

# delete("coffee")
# delete("orange")
# delete("water")
# delete("tea")

update("coffee", 2, 10)

print("*** End ***")
print (view())
